import {createBrowserRouter, Navigate, RouterProvider} from "react-router-dom";


import Login from "./pages/Login";
import Register from "./pages/Register";
import User from "./pages/User";
import ErrorPage from "./error-page";
import Blog from "./pages/Blog";
import { Header } from "./components/header/header";
import { Footer } from "./components/footer/footer";

const router = createBrowserRouter([
    {
      path: "/",
      element: <Navigate to="login"/>,
      errorElement: <ErrorPage/>,  
    },
  
    {
      path: "/",
      element: <Navigate to="login"/>,
    },
    {
      path: "/login",
      element: <Login/>,
    },
    {
      path: "/register",
      element: <Register/>,
    },
    {
      path: "/user",
      element: <User/>,
    },
    {
      path: "/blog",
      element: <Blog/>,
    },
]);

export default function App() {
    return (
        
        <div className="wrapper">
        
          <Header/>
          <RouterProvider router={router}/>
          <Footer/>
       
        </div>
    )
}