import { initializeApp } from "firebase/app";
import {createUserWithEmailAndPassword, signInWithEmailAndPassword, getAuth} from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBdjjc2Xt1P2eP40-rQir-31VgjiEfxhmQ",
  authDomain: "react-gl.firebaseapp.com",
  projectId: "react-gl",
  storageBucket: "react-gl.appspot.com",
  messagingSenderId: "420721271443",
  appId: "1:420721271443:web:714b4549cbac61bdf3e366"
};

const app = initializeApp(firebaseConfig);

export const createUser = async (email, password) => {
  return createUserWithEmailAndPassword(getAuth(app), email, password);
}

export const signInUser = async (email, password) => {
  return signInWithEmailAndPassword(getAuth(app), email, password);
}