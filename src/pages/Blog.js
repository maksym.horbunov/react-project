import React, { useState, useEffect } from 'react';

import Form from "../components/form/form";
import Posts from "../components/posts/posts";

export default function Blog() {

  const [products, setProducts] = useState([]);

  async function fetchProducts() {
    const response = await fetch("https://rickandmortyapi.com/api/character");
    let productsArray,
        productsArrayResult;
    if (response.status >= 200 && response.status <= 299) {
        productsArray = await response.json();
        productsArrayResult = productsArray.results;
    } else {
        productsArrayResult = [];
    }
    setProducts(productsArrayResult);
}

const postAdd = (e) => {
  setProducts([...products, e])
};

useEffect(() => {fetchProducts()}, []);

  return (
    <div className="container">
      <h1>Blog</h1>
      <Form emitAdd={postAdd}></Form>  

      <div className="product-list">
          {products.map(product => 
            <Posts product={product} key={product.id}/>
          )}
      </div>
    </div>
  );
}