import {Container, Box, TextField, Button} from "@mui/material";
import React, { useState } from 'react';

export default function Form(props) {


    const [newProducts, setNewProducts] = useState({"name":" ","status":" "});

    const newPost = () => {
      props.emitAdd(newProducts)
    };

  return (
    <Container maxWidth="xs" sx={{mt: 2}}>
        <Box component="form">
          <TextField
            label="Blog title"
            variant="outlined"
            sx={{mt: 1}}
            fullWidth
            onChange={event => setNewProducts(newProducts => ({ ...newProducts, name: event.target.value }))}
          />

          <TextField
            label="Blog description"
            variant="outlined"
            sx={{mt: 1}}
            fullWidth
            multiline
            onChange={event => setNewProducts(newProducts => ({ ...newProducts, status: event.target.value }))}
          />
        </Box>
        
        <Button onClick={newPost} variant="contained" type="submit" sx={{mt: 3}} fullWidth>Add</Button>
    </Container>
  )
}