import {Link} from "@mui/material";
import styles from "./header.css";


export const Header = () => {
  return (
    <header>
      <div className="container">
        <ul>
            <li>
              <Link href="/Blog">About</Link>
            </li>

            <li>
              <Link href="/Blog">Contacts</Link>  
            </li>
        </ul> 
      </div>
    </header>
  );
};